import tkinter as tk

root= tk.Tk()

canvas1 = tk.Canvas(root, width = 300, height = 300)
canvas1.pack()

def hello ():  
    label1 = tk.Label(root, text= 'Hello World!', fg='green', font=('helvetica', 12, 'bold'))
    canvas1.create_window(150, 200, window=label1)
    
button1 = tk.Button(text='Click Me',command=hello, bg='brown',fg='white')
canvas1.create_window(150, 150, window=button1)

root.mainloop()
####################################
import sys
sys.path.insert(1,"./libs")
import time
from RockClient import *
import tkinter as tk

root= tk.Tk()

def getter():
    message = r.get_message()

    if message is not None:  
        print("Processing messages...")
        r.process_message(message) 

def emergency():
    r.emergency_meeting()

def sender():
    # ASK user what message wants to send:
    while(True):

        res = str(input("Type the message you want to send:"))

        try:
            if( len(res) < 111 ):
                print("Apparently correct typed message.")
                msg_text = res
                r.send_msg(msg_text, False)
                break
            print("ERROR: text length is ", len(res) ," bytes and should be 111 bytes maximum.\n")
            print("Try to erase", ( len(res)-111 ), "bytes from your text")

        except Exception as e:
            print("ERROR:",e)

def rtl():
    r.execute_rtl()

# def clearer():
#     r.clear_buffer(clearer)

if __name__ == "__main__":

    canvas1 = tk.Canvas(root, width = 600, height = 600)
    canvas1.pack()
    
    r = RockBlocks(canvas1)

    button_getter = tk.Button(text='Get message',command=getter, bg='green',fg='white')
    canvas1.create_window(150, 150, window=button_getter)

    button_sender = tk.Button(text='Send message',command=sender, bg='brown',fg='white')
    canvas1.create_window(150, 450, window=button_sender)

    button_rtl = tk.Button(text='RTL',command=rtl, bg='blue',fg='white')
    canvas1.create_window(450, 150, window=button_rtl)
    
    button_emergency = tk.Button(text='Emergency',command=emergency, bg='red',fg='white')
    canvas1.create_window(450, 450, window=button_emergency)

    root.mainloop()

        
        

        