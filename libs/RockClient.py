import serial
from serial.tools.list_ports import comports
import time
import sys
import os
import json
import glob
import keyboard  # using module keyboard
from pynput.keyboard import Key, Controller
from adafruit_rockblock import *

class RockBlocks:
    def __init__(self, filename = None):
        # RockBlock configuration and connection:
        self.config = self.configure(filename)
        self.keyboard = Controller()       
        config = self.config['parameters']

        self.baudrate = config["rb_baud"]
        self.port = config["rb_com"]
        # self.drone_sn = config["drone_sn"]
        self.gcs_sn = config["GCS_rb_sn"]

        self.rb = self.ensure_connection()

    def ensure_connection(self):
        print("Ensuring connection...\n")
        baudrate = self.baudrate
        port1 = self.port

        # Make sure the user has connected the RB:
        ports = list(comports()) 
        rb_port = False
        
        for p in ports:
            
            if port1 in p:
                rb_port = True
            else:
                rb_port = False
        
        # RB Port correct 
        if (rb_port == True):
            uart = serial.Serial(port1, baudrate)
            rb = RockBlock(uart)
            print("RockBlock connected!\n")
            return rb

        # RB Port not correct 
        elif (rb_port == False):
            port2 = (self.select_RB_port())

            # RB Port correction in config 
            self.config['parameters']['rb_com'] = port2

            a_file = open("config.json", "w")
            json.dump(self.config, a_file)

            uart = serial.Serial(port2, baudrate)
            rb = RockBlock(uart)
            print("RockBlock connected!\n")
            return rb

    def configure(self,filename):
        # Recoje la información del archivo de la configuración
        if not filename:
            filename = "config.json" # a no ser que le cambiemos el nombre del archivo, va a utilizar el que creamos

        try:
            with open( filename, "r") as fd:
                cfg = json.load(fd)
                return cfg #parte del json que tiene la info

        except FileNotFoundError:
            return None

    def select_RB_port(self):
        ports = list(comports()) 
        port_name = ""
            
        while len(ports) == 0:
            print("Please connect the RockBlock...")
            time.sleep(5)
            ports = list(comports())  
                    
        # if len(ports) > 0:
        #     print("\nPORT number | PORT name")
        #     for port in ports:
        #         print( "   ",ports.index(port)+1, "      :" , port , "\n")

        #         if port[2].startswith("USB VID:PID=0403:6001 SER=FTB"):
                    
        #             port_name = str(port[0])
                    
        if len(ports) > 0:
            print("\nList number : PORT")
            for port in ports:
                print( "   ",ports.index(port), "      :" , port , "\n")

            #print("You can check RB port nº at: Administrador de dispositivos/Puertos COM y LPT \n")
            res = ""
            while True:
                try:
                    res = int(input("Type the list number corresponding to RockBlock's port: "))

                    if (ports[res] in ports):
                        port = list(ports[res])
                        port_name = port[0]
                        print("\nPort", port_name ,"has been selected\n")
                        break
                    print("ERROR: No list number", res ," has been found. \n")
                except Exception as e:
                    print("ERROR:",e)
            
        port = list(ports[res])
        port_name = port[0]
        # print("Port:", port)
        # print("Port name:", port_name)
        return port_name
    
    def get_time(self):
        '''If satellite connection, returns UTC timestamp with y_m_d-h_m format'''
        rb = self.rb
        resp = rb._uart_xfer("+CCLK?")  # 20/09/26,12:07:13

        if resp[-1].strip().decode() == "OK":
            status = tuple(resp[1].decode().split(","))
            date = (status[0].split(":"))[1]

            year = str(int(date.split("/")[0]) + 2000)
            month = date.split("/")[1]
            day = date.split("/")[2]

            time = status[1]
            hour = int(time.split(":")[0]) # UTC 
            min = time.split(":")[1]
            # sec = time.split(":")[2]

            timestamp = str(year) + "_" + str(month) + "_" + \
                str(day) + "-" + str(hour) + "_" + str(min)
            # 2020_09_26-12_07
            return timestamp
    
    def check_connection(self, rb):

        resp = rb._uart_xfer("+CSQ")

        if resp[-1].strip().decode() == "OK":
            status = int(resp[1].strip().decode().split(":")[1])

        else:
            quality = False

        signal_strength = status

        print("Signal strength:", signal_strength)

        if signal_strength >= 1:
            quality = True

        else:
            quality = False

        return quality  

    def send_msg(self, msg, drone):

        if drone:
            text = self.codification(drone,msg) #THIS IS FOR MESSAGES SENT BY DRONE!
        else:
            text = msg

        if text is not None:
            data = text
            cc = self.check_connection(self.rb)
        
            previous = time.perf_counter()
            timer = 0

            while cc is not True:
                current = time.perf_counter()
                timer += current - previous
                previous = current

                print("Checking again...\n")
                cc = self.check_connection(self.rb)

                if timer > 15:
                    print('Im tired of checking signal\n')
                    break

            if cc is not False:
                print("Ready to send message!")

                # put data in outbound buffer
                self.rb.text_out = data

                # try a satellite Short Burst Data transfer
                print("Talking to satellite...\n")

                status = self.rb.satellite_transfer()
                print("Try num:", 1)
                print("Satellite status:", status)

                if( status[0] > 8 ):
                    print("The communication has failed")
                else:
                    print("\nDONE.")

        elif text is None:
            print("Try again...\n") 

        return None
       
    def buffer_status(self,buffer):
        """Current state of the mobile originated and mobile terminated buffers"""
        rb = self.rb
        status = (None,) * 4 # # [MOflag,MOmsn,MTflag,MTmsn]
        bufStatus = 0
        resp = rb._uart_xfer("+SBDS")
        if resp[-1].strip().decode() == "OK":
            status = resp[-3].strip().decode().split(":")[1]
            status = [int(s) for s in status.split(",")]
        status = tuple(status)
        if buffer == "MT":
            bufStatus = status[2]
        elif buffer == "MO":
            bufStatus = status[0]

        return bufStatus

    def get_message(self):
        rb = self.rb
        cc = self.check_connection(rb)
        
        previous = time.perf_counter()
        timer = 0
        message = []

        while cc is not True:
            current = time.perf_counter()
            timer += current - previous
            previous = current

            print("Checking again...\n")
            cc = self.check_connection(rb)

            if timer > 15:
                print('Im tired of checking signal\n')
                break

        if cc is not False:
            messages = []
            # Check current MT buffer:
            MTQueue = self.buffer_status("MT")

            if MTQueue == 0:
                # try a satellite Short Burst Data transfer
                print("Talking to satellite...\n")
                status = rb.satellite_transfer()
                print("Try 1:",status) 
                
                if status[0] > 8:
                    print("The communication has failed\n")
                else:
                    print("\nDONE.")   
                    MTQueue = self.buffer_status("MT")

                if MTQueue == 0:
                    print("No messages received.\n")
                    messages = None
  
            while MTQueue >= 1:
                # get the text from the MT buffer
                message = rb.text_in
                if message.startswith("RB"): 
                    message = message[9:] # 'RB0016777RTL'
                messages.append(message)
                MTQueue = MTQueue - 1  
                print("Remaining MT messages: ", MTQueue) 

            return messages

    def process_message(self, msg):
        i = 0
        if msg is not None:
            print("Processing messages...\n")
            while i < len(msg):
                data = self.decodification(msg[i])
                print(json.dumps(data, indent = 4))
                i += 1
        else:
            print("No messages processed.\n")
        
        return None

    def codification(self,drone, msg):
        print("Codification...\n")
        sn = self.config['drones'][drone]
    
        # Ensure Serial number is 7 bytes:
        if len(sn) < 7:
            zeros = 7 - len(sn)
            while zeros > 0:
                sn = "0" + sn
                zeros = zeros - 1
            
            # RB serial number correction in config: 
            self.config['parameters']['drone_sn'] = sn
            a_file = open("config.json", "w")
            json.dump(self.config, a_file)

            # Write text message with correct prefix:
            prefix = "RB" + sn
            txt = prefix + msg     
            # txt = str.encode(txt)   
            return txt

        elif len(sn) == 7:
            # Write text message with correct prefix:
            prefix = "RB" + sn
            txt = prefix + msg     
            # txt = str.encode(txt)   
            return txt
            
        elif len(sn) > 7:
            print("ERROR: Serial number has more than 7 characters, check it at config.json\n")      
            return None
    
    def decodification(self, msg):

        if msg.startswith("RB"): 
            msg = msg[9:]

        values = msg.split(",")
        landing = 0
        flying = 1
        sos = 2   #"R7,2,..."
        rtl = 3   #"droneid,3,Rtl is being executed"  
        drone_id = values[0]
        status = int(values[1])
        
        data = {}

        
        
        if status == landing: 
            lat = (int(values[2])) / 100000
            lon = (int(values[3])) / 100000
            alt = (int(values[4])) / 10
            flight_time = values[5]
            data['Landing'] = []
            data['Landing'].append({'drone_id': drone_id,
                'status':"Landing",
                'latitude': lat,
                'longitude': lon,
                'altitude': alt,
                'mission time': flight_time})  

        elif status == flying:
            lat = (int(values[2])) / 100000
            lon = (int(values[3])) / 100000
            alt = (int(values[4])) / 10
            flight_time = values[5]
            heading = values[6]
            mission_type = values[7]
            data['Flying'] = []
            data['Flying'].append({
                'drone_id': drone_id,
                'status':"Flying",
                'latitude': lat,
                'longitude': lon,
                'altitude': alt,
                'mission time': flight_time,
                'heading': heading,
                'mission': mission_type  
            } )

        elif status == sos:
            message = values[2]
            data['SOS'] = []
            data['SOS'].append({
                'drone_id': drone_id,
                'status': "SOS message response",
                'message': message   
            } )

        elif status == rtl:
            message = values[2]
            data['RTL'] = []
            data['RTL'].append({
                'drone_id': drone_id,
                'status': "RTL drone response",
                'message': message   
            } )

        return data
    
    def check_sender(self):
        # SENDER:
        sender = False
        #used try so that if user pressed other than the given key error will not be shown
        if keyboard.is_pressed('s'):  # if key 's' is pressed 
            
            print('Sender has been activated...\n')
            sender  = True
            self.keyboard.press(Key.backspace)

        else:
            sender = False

        return sender

    def check_getter(self):
        getter = False
           
        #used try so that if user pressed other than the given key error will not be shown
        if keyboard.is_pressed('g'):  # if key 's' is pressed 
            
            print('Getter has been activated...\n')
            getter  = True
            self.keyboard.press(Key.backspace)
        else:
            getter = False

        return getter

    def check_clear(self):
        clearer = False    
        #used try so that if user pressed other than the given key error will not be shown
        if keyboard.is_pressed('o'): 
            print('MO message buffer cleaning has been activated...\n')
            clearer  = 0
            self.keyboard.press(Key.backspace)

        elif keyboard.is_pressed('t'): 
            print('MT message buffer cleaning has been activated...\n')
            clearer  = 1
            self.keyboard.press(Key.backspace)
        elif keyboard.is_pressed('b'): 
            print('Clearer has been activated...\n')
            clearer  = 2
            self.keyboard.press(Key.backspace)
        else:
            clearer = None
                    
        return clearer

    def check_emergency(self):
        emergency = False    
        #used try so that if user pressed other than the given key error will not be shown
        if keyboard.is_pressed('e'): 
            print('Emergency meeting has been activated...\n')
            emergency = True
            self.keyboard.press(Key.backspace)
        else:
            emergency = False

        return emergency
        
    def check_rtl(self):
        # RTL:
        rtl = False
        #used try so that if user pressed other than the given key error will not be shown
        if keyboard.is_pressed('r'):  # if key 's' is pressed 
            
            print('RTL button has been activated...\n')
            rtl  = True
            self.keyboard.press(Key.backspace)
        else:
            rtl = False

        return rtl
        
    def clear_buffer(self, deletetype):
        """ Clear the mobile originated buffer, mobile terminated buffer or both. """
        # Delete type: 
        #     1. MO messages: 0
        #     2. MT messages: 1
        #     3. Both: 2

        rb = self.rb
        
        resp = rb._uart_xfer( "+SBDD" + str(deletetype) )
        res = resp[1].decode()

        if res == 0:
            print("Buffer(s) cleared succesfully.")
        else:
            print("An error occurred while clearing the buffer(s).")

    def emergency_meeting(self):
        drone_id = self.select_drone()
        msg = "We need to know the " + str(drone_id) + " last location"
        self.send_msg(msg, False)

    def execute_rtl(self):
        msg = "RTL"
        drone = self.select_drone()

        self.send_msg(msg, drone)
    
    def select_drone(self):

        drones = self.config['drones']
        drone_list = []
        i = 0

        print("\nDRONE number | DRONE name")
       
        for drone in drones:
            print( "   ",i, "      :  " , drone , "\n")
            drone_list.append(drone)
            i += 1

        res = ""
        while True:
            try:
                res = int(input("Type the DRONE number to select it:"))

                if (res < len(drone_list)):
                    drone_name = drone_list[res]
                    print("DRONE", drone_name ,"has been selected.")
                    break
                print("ERROR: No DRONE number", res ," has been found.")
            except Exception as e:
                print("EXCEPTION:",e)
            
        return drone_name

    def check_menu(self):
        # MENU:
        menu = False
        #used try so that if user pressed other than the given key error will not be shown
        if keyboard.is_pressed('m'):  # if key 's' is pressed 
            
            print('Menu button has been activated...\n')
            menu  = True
            self.keyboard.press(Key.backspace)
        else:
            menu = False

        return menu