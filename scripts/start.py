import sys
sys.path.insert(1,"./libs")
import time
from RockClient import *

if __name__ == "__main__":
    
    r = RockBlocks()    
    
    # Future implementations:
    # print("Press O at Keyboard if you want to clear buffer MO messages.")
    # print("Press T at Keyboard if you want to clear buffer MT messages.")
    # print("Press B at Keyboard if you want to clear both buffer messages.")
    
    print("Press E at Keyboard if you want an Emergency meeting")
    print("Press R at Keyboard if you want to execute RTL")
    print("Press S at Keyboard if you want to send a message.")
    print("Press G at Keyboard if you want to get a message.")
    print("Press M at Keyboard if you want to return to menu.")
    
    while True:
        
        getter = r.check_getter()
        sender = r.check_sender()
        emergency = r.check_emergency()
        rtl = r.check_rtl()
        menu = r.check_menu()
        # clearer = r.check_clear()

        if menu:
            print("Press E at Keyboard if you want an Emergency meeting")
            print("Press R at Keyboard if you want to execute RTL")
            print("Press S at Keyboard if you want to send a message.")
            print("Press G at Keyboard if you want to get a message.")
            print("Press M at Keyboard if you want to return to menu.")
        
        if emergency:
                r.emergency_meeting()
            
            # while(True):

                # res = str(input("Type the drone id to receive its last location:"))
                
                # try:
                #     if( len(res) < 79 ):
                #         print("Apparently correct typed message.")
                #         drone_id = res
                #         r.emergency_meeting(drone_id)
                #         break

                #     print("ERROR: text length is ", len(res) ," bytes and should be 111 bytes maximum.\n")
                #     print("Try to erase", ( len(res)-79 ), "bytes from your text")

                # except Exception as e:
                #     print("ERROR:", e)

        if getter:
            message = r.get_message()

            if message is not None:  
                print("Processing messages...\n")
                r.process_message(message) 
                   
        if sender:
            # ASK user what message wants to send:

            while(True):

                res = str(input("Type the message you want to send: "))

                try:
                    if( len(res) < 111 ):
                        print("Apparently correct typed message.\n")
                        msg_text = res
                        r.send_msg(msg_text, False)
                        break
                    print("ERROR: text length is ", len(res) ," bytes and should be 111 bytes maximum.\n")
                    print("Try to erase", ( len(res)-111 ), "bytes from your text\n")

                except Exception as e:
                    print("ERROR: ",e)

        # if clearer is not None:
        #     r.clear_buffer(clearer)

        if rtl:
            r.execute_rtl()
        
        